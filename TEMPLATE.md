
ROLE_NAME='baosystems.postgresql'
ROLE_DESC='PostgreSQL'
YOUR_NAME='Alan Ivey'


grep -l -r baosystems\.template . | xargs -L 1 --no-run-if-empty sed -e "s/baosystems\.template/$ROLE_NAME/g" -i

sed \
  -r \
  -e "/^\s+author:/ s/:.*/: $YOUR_NAME/" \
  -e "/^\s+description:/ s/:.*/: $ROLE_DESC/" \
  -e "s/baosystems\.template/$ROLE_NAME/g" \
  meta/main.yml

# Also in meta/main.yml:
# * Add additional galaxy_tags if necessary
# * Change license to MIT if open source

# If intended to be open source, replace ./LICENSE contents with that from https://choosealicense.com/licenses/mit/
# Be sure to replace '[year]' with the current year or a range from the first year of work to the current year, such
# as '2020' or '2019-2020'.
# And, replace '[fullname]' with 'BAO Systems'.

# Separately, create Bitbucket repo empty. Don't create default license/readme/etc. Main branch develop, master to be used for tagging.
