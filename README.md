baosystems.postgresql
=====================

Install PostgreSQL from the PGDG repositories. Supports PostgreSQL 10 and higher on CentOS 7.


Requirements
------------

Developed against Ansible 2.9.


Role Variables
--------------

* `postgresql_install_server` - Boolean. If `true`, installs and configures PostgreSQL server and client. If `false`, only the PostgreSQL client is installed. Default is `true`.

* `postgresql_version` - Integer. Supported values are `10`, `11`, `12`, and `13`. Default is `13`.


Dependencies
------------

Requires [geerlingguy.repo-epel](https://galaxy.ansible.com/geerlingguy/repo-epel) and [geerlingguy.postgresql](https://galaxy.ansible.com/geerlingguy/postgresql) from Ansible Galaxy. See [requirements.yml](https://bitbucket.org/baosystems/baosystems.postgresql/src/develop/molecule/shared/common/requirements.yml).


Example Playbook
----------------

```yaml
---
- hosts: all
  tasks:
    - include_role:
        name: baosystems.postgresql
      vars:
        postgresql_install_server: true
        postgresql_version: 13
```


License
-------

MIT


Author Information
------------------

* [Alan Ivey](https://github.com/alanivey/), BAO Systems
