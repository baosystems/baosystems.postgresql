### Requires "direnv", see http://direnv.net
### and, `brew install direnv` to get started



###############################################################################
# Pre-Pipenv Checks

if ! command -v pipenv >/dev/null 2>&1 ; then
  echo -e '\033[0;31m'[ERROR]'\033[0m' Unable to locate pipenv
  return 1
fi



###############################################################################
# Pipenv


# Allow direnv to use the virtualenv from Pipenv; see https://github.com/direnv/direnv/wiki/Python#pipenv
layout pipenv



###############################################################################
# Post-Pipenv Checks

# Notify if non-virtualenv binaries are not available
if ! command -v npm >/dev/null 2>&1 ; then
  echo -e '\033[0;31m'[ERROR]'\033[0m' Unable to locate npm
  return 1
fi
if ! command -v npx >/dev/null 2>&1 ; then
  echo -e '\033[0;31m'[ERROR]'\033[0m' Unable to locate npx
  return 1
fi

# Notify if virtualenv binaries are not available
if ! command -v pre-commit >/dev/null 2>&1 ; then
  echo -e '\033[0;31m'[ERROR]'\033[0m' Unable to locate pre-commit
  return 1
fi

# Install missing node modules
if \
  ! npm list --depth 0 "@commitlint/config-conventional" >/dev/null 2>&1 || \
  ! npm list --depth 0 "conventional-changelog-conventionalcommits" >/dev/null 2>&1 || \
  ! npm list --depth 0 "cz-conventional-changelog" >/dev/null 2>&1
then
  echo -e '\033[1;33m'[WARNING]'\033[0m' Unable to verify npm modules, installing...
  npm_config_loglevel=error npm --no-fund ci
fi

# Install missing local git hooks
if [ ! -e .git/hooks/commit-msg ] ; then
  echo -e '\033[1;33m'[WARNING]'\033[0m' File .git/hooks/commit-msg does not exist, installing...
  pre-commit install --hook-type commit-msg
fi
if [ ! -e .git/hooks/pre-commit ] ; then
  echo -e '\033[1;33m'[WARNING]'\033[0m' File .git/hooks/pre-commit does not exist, installing...
  pre-commit install
fi
if [ ! -e .git/hooks/pre-push ] ; then
  echo -e '\033[1;33m'[WARNING]'\033[0m' File .git/hooks/pre-push does not exist, installing...
  pre-commit install --hook-type pre-push
fi

# Install missing git alias
if ! git config --local alias.cz >/dev/null 2>&1 ; then
  echo -e '\033[1;33m'[WARNING]'\033[0m' Local git alias 'cz' does not exist, installing...
  git config --local alias.cz '!npx --quiet git-cz'
fi

# Include tags with pull
if ! git config --local alias.pull 'pull --tags' >/dev/null 2>&1 ; then
  echo -e '\033[1;33m'[WARNING]'\033[0m' Local git alias 'pull' does not exist, installing...
  git config --local alias.pull 'pull --tags'
fi

# Include tags with push
if ! git config --local push.followTags >/dev/null 2>&1 ; then
  echo -e '\033[1;33m'[WARNING]'\033[0m' Local git config 'push.followTags' does not exist, installing...
  git config --local push.followTags true
fi



###############################################################################
# Local overrides

## Include local changes that are not included in the git repository
if [[ -r .envrc.local ]]; then
  source_env .envrc.local
fi
