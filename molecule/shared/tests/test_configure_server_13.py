import pytest


# The value of v should be the only difference in this file compared to others of similar name.
v = '13'


def test_pg_ident(host):
    f = host.file(f'/var/lib/pgsql/{v}/data/pg_ident.conf')
    assert f.user == 'postgres'
    assert f.group == 'postgres'
    assert f.mode == 0o600

    content = f.content_string
    assert 'local_users      postgres           postgres' in content
    assert 'local_users      root               postgres' in content


def test_postgresqlconf_listen(host):
    content = host.file(f'/var/lib/pgsql/{v}/data/postgresql.conf').content_string
    assert "listen_addresses = '*'" in content
    assert "include_dir 'conf.d'" in content


def test_confddir(host):
    d = host.file(f'/var/lib/pgsql/{v}/data/conf.d')
    assert d.is_directory
    assert d.user == 'postgres'
    assert d.group == 'postgres'
    assert d.mode == 0o0700


def test_confd_01logging(host):
    f = host.file(f'/var/lib/pgsql/{v}/data/conf.d/01-logging.conf')
    assert f.user == 'postgres'
    assert f.group == 'postgres'
    assert f.mode == 0o600

    # Checking for 16 option/value pairs is slow. A checksum is sufficient for a file
    # that is not an Ansible template. Obtain via:
    # $ sha256sum files/var/lib/pgsql/version/data/conf.d/01-logging.conf
    assert f.sha256sum == '706f3b56a03877327d1b6eb35042f636cfda049ee5b12a5504c5d0e74001ef63'


def test_logrotate(host):
    f = host.file(f'/etc/logrotate.d/postgresql-{v}')
    assert f.is_file

    content = f.content_string
    # "In order to make a brace appear in your string, you must use double braces"
    # Source: https://realpython.com/python-f-strings/#braces
    assert f'/var/lib/pgsql/{v}/data/pg_log/postgresql.log {{' in content
    assert f'/var/lib/pgsql/{v}/pgstartup.log {{' in content


def test_pg_log(host):
    secheck = host.run('/usr/sbin/getenforce')
    if not secheck.stdout == 'Enforcing\n':
        pytest.skip('requires SELinux to be "Enforcing"')

    cmd = host.run(f'/usr/bin/stat -c %C /var/lib/pgsql/{v}/data/pg_log')
    assert cmd.stdout.split(':')[0] == 'system_u'
    assert cmd.stdout.split(':')[1] == 'object_r'
    assert cmd.stdout.split(':')[2] == 'postgresql_log_t'
