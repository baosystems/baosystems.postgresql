import pytest


def test_epel(host):
    p = host.package('epel-release')
    assert p.is_installed


def test_pgdg_key_aarch64(host):
    if not host.system_info.arch == 'aarch64':
        pytest.skip('requires platform arch aarch64')

    p = host.package('gpg-pubkey-6d960b89-5f368958')
    assert p.is_installed


def test_pgdg_key(host):
    p = host.package('gpg-pubkey-73e3b907-6581b071')
    assert p.is_installed


def test_pgdg_pkg(host):
    p = host.package('pgdg-redhat-repo')
    assert p.is_installed


def test_postaction_pkg(host):
    p = host.package('yum-plugin-post-transaction-actions')
    assert p.is_installed


def test_postaction_actionfile(host):
    f = host.file('/etc/yum/post-actions/postgresql.action')
    assert f.is_file
    # @TODO: test for contents to match, checksum?


def test_pythonlib_pkg_old(host):
    p = host.package('python2-psycopg2')

    if p.is_installed:
        assert not p.version.startswith('2.7.')
