import re


def test_pgpass(host):
    postgres_userhome = host.run('/usr/bin/getent passwd postgres').stdout.split(':')[5]

    f = host.file(f'{postgres_userhome}/.pgpass')
    assert f.is_file

    content = f.content_string
    # For the special characters list, - and ] are escaped.
    assert re.findall(r'^localhost:5432:'
                      r'\*:postgres:[a-zA-Z0-9#&()+,\-.<=>?[\]^_~]{127}\n', content)
    assert re.findall(r'\n127\.0\.0\.1:5432:'
                      r'\*:postgres:[a-zA-Z0-9#&()+,\-.<=>?[\]^_~]{127}\n', content)
    assert re.findall(r'\n\\:\\:1:5432:'
                      r'\*:postgres:[a-zA-Z0-9#&()+,\-.<=>?[\]^_~]{127}\n', content)

    t = host.file(f'{postgres_userhome}/.pgpass.new')
    assert not t.exists
