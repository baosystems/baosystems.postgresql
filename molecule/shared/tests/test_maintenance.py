import pytest


@pytest.mark.parametrize('pkg', [
    'cpio',
    'diffutils',
    'rpm',
])
def test_maintenance_restorepgdgrepo(host, pkg):
    if not host.system_info.arch == 'x86_64':
        pytest.skip('tests only applicable on platform arch x86_64')

    p = host.package(pkg)
    assert p.is_installed

    f = host.file('/etc/yum/post-actions/pgdg-redhat-repo.action')
    assert not f.exists

    cmd = host.run('/usr/bin/rpm2cpio https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm'  # noqa E501
                   '| /usr/bin/cpio --quiet --extract --to-stdout ./etc/yum.repos.d/pgdg-redhat-all.repo'  # noqa E501
                   '| /usr/bin/diff - /etc/yum.repos.d/pgdg-redhat-all.repo')
    assert cmd.succeeded
