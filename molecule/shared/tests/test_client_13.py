# The value of v should be the only difference in this file compared to others of similar name.
v = '13'


def test_client_pkg(host):
    p = host.package(f'postgresql{v}')
    assert p.is_installed
