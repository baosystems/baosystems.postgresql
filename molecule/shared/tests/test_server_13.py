import re
import pytest


# The value of v should be the only difference in this file compared to others of similar name.
v = '13'


@pytest.mark.parametrize('pkg', [
    f'postgresql{v}',
    f'postgresql{v}-contrib',
    f'postgresql{v}-libs',
    f'postgresql{v}-server',
])
def test_server_pkgs(host, pkg):
    p = host.package(pkg)
    assert p.is_installed


def test_pythonlib_pkg(host):
    p = host.package('python2-psycopg2')
    assert p.is_installed


def test_env_file(host):
    f = host.file('/etc/profile.d/postgres.sh')
    assert f.is_file
    assert f.mode == 0o644

    content = f.content_string
    assert f'export PGDATA=/var/lib/pgsql/{v}/data' in content
    assert f'export PATH=$PATH:/usr/pgsql-{v}/bin' in content


def test_datadir(host):
    d = host.file(f'/var/lib/pgsql/{v}/data')
    assert d.is_directory
    assert d.user == 'postgres'
    assert d.group == 'postgres'
    assert d.mode == 0o0700


def test_pgversion_file(host):
    f = host.file(f'/var/lib/pgsql/{v}/data/PG_VERSION')
    assert f.is_file
    assert f.user == 'postgres'
    assert f.group == 'postgres'
    assert f.mode == 0o600

    content = f.content_string
    assert v in content


@pytest.mark.parametrize('option, value', [
    ('shared_buffers', '8MB',),
    ('logging_collector', 'off',),
    ('log_filename', 'postgresql-%Y-%m-%d_%H%M%S.log',),
    ('log_truncate_on_rotation', 'off',),
    ('log_rotation_size', '10MB',),
    ('datestyle', 'ISO, MDY',),
    ('default_text_search_config', 'pg_catalog.simple',),
    ('log_timezone', 'UTC',),
    ('timezone', 'UTC',),
    ('unix_socket_directories', '/var/run/postgresql',),
])
def test_postgresqlconf(host, option, value):
    f = host.file(f'/var/lib/pgsql/{v}/data/postgresql.conf')
    assert f.is_file
    assert f.user == 'postgres'
    assert f.group == 'postgres'
    assert f.mode == 0o600

    content = f.content_string
    assert f"{option} = '{value}'" in content


def test_pg_hba(host):
    f = host.file(f'/var/lib/pgsql/{v}/data/pg_hba.conf')
    assert f.user == 'postgres'
    assert f.group == 'postgres'
    assert f.mode == 0o600

    content = f.content_string
    assert re.findall(r"local\s+all\s+all\s+peer map=local_users", content)
    assert re.findall(r"host\s+all\s+all\s+127\.0\.0\.1/32\s+md5", content)


def test_socketdir(host):
    d = host.file('/var/run/postgresql')
    assert d.is_directory
    assert d.user == 'postgres'
    assert d.group == 'postgres'
    assert d.mode == 0o755


def test_service(host):
    s = host.service(f'postgresql-{v}')
    assert s.is_enabled
    assert s.is_running


def test_socket(host):
    assert host.socket('tcp://0.0.0.0:5432').is_listening
