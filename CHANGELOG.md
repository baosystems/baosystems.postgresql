# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.1.0%0D1.0.8#diff) (2024-01-09)


### Features

* update locations of pgdg gpg public keys ([53f4cc4](https://bitbucket.org/baosystems/baosystems.postgresql/commits/53f4cc41a7343d8bc94e2a557ee6d6f875fedadc))


### Tests

* new gpg key ([a2f5859](https://bitbucket.org/baosystems/baosystems.postgresql/commits/a2f5859313d0c1c50b9ef45a6c49bbf02cf6ede4))

## [1.0.8](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.8%0D1.0.7#diff) (2022-11-15)


### Bug Fixes

* update checksum ([3e1efdb](https://bitbucket.org/baosystems/baosystems.postgresql/commits/3e1efdb12dbf75d5b8daf0f7bd5d1f78bd2c4ce9))

## [1.0.7](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.7%0D1.0.6#diff) (2022-02-25)


### Bug Fixes

* ok for pgstartup.log to be missing ([198d7cf](https://bitbucket.org/baosystems/baosystems.postgresql/commits/198d7cfd4281a4e753e274528ef1c45de6050abe))

## [1.0.6](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.6%0D1.0.5#diff) (2021-10-04)


### Bug Fixes

* update ca-certificates ([61ed66b](https://bitbucket.org/baosystems/baosystems.postgresql/commits/61ed66b6df608bdcf1bcc877e8fb666a971971b1))


### Others

* remove CentOS 8 logic and references ([1ff134e](https://bitbucket.org/baosystems/baosystems.postgresql/commits/1ff134ed3ba8c7448951d57c943f364f842d3bec))
* remove extra platforms ([fd3cd89](https://bitbucket.org/baosystems/baosystems.postgresql/commits/fd3cd89fa9849cbc2461a9da19d0b7f090d8dc12))
* remove PIPENV_VENV_IN_PROJECT from .envrc ([0682356](https://bitbucket.org/baosystems/baosystems.postgresql/commits/06823563b419243a780c008eca86ecde8532b63b))
* remove unnecessary section ([d398bcc](https://bitbucket.org/baosystems/baosystems.postgresql/commits/d398bcc0cb201f3c3fb44a75966c42c0de9aa4fe))


### CI

* don't use 2x ([200e0f9](https://bitbucket.org/baosystems/baosystems.postgresql/commits/200e0f913ee442f9aadbcab44bcc58a794401687))
* ensure single key attempted for ssh ([4fa99c2](https://bitbucket.org/baosystems/baosystems.postgresql/commits/4fa99c28af3db9f57f97214d843786a1ca278f0f))
* test version 13 by default ([d469869](https://bitbucket.org/baosystems/baosystems.postgresql/commits/d4698694397b53e7f71975135e9fa0bca8e8a2f2))
* update pipenv ([294a454](https://bitbucket.org/baosystems/baosystems.postgresql/commits/294a45495f10ff3c18091f1089852826ad11fc20))


### Tests

* skip ansible-lint 403 ([498bf4f](https://bitbucket.org/baosystems/baosystems.postgresql/commits/498bf4f9cb34fee0abab03c8876c06bdd78a219e))
* support for postgresql 14 ([2744963](https://bitbucket.org/baosystems/baosystems.postgresql/commits/2744963711cb31679c14f058ee746395cd09fec1))
* update upstream gpg key ([4d7b288](https://bitbucket.org/baosystems/baosystems.postgresql/commits/4d7b28853227ae082d942238d8a440e224a431b1))

## [1.0.5](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.5%0D1.0.4#diff) (2020-12-29)


### Bug Fixes

* ensure python2-psycopg2 is not old ([5ceb6c1](https://bitbucket.org/baosystems/baosystems.postgresql/commits/5ceb6c14719c25aaa4c82e76c78194116b910fe6))
* update PowerTools repo logic ([ab8e75a](https://bitbucket.org/baosystems/baosystems.postgresql/commits/ab8e75a2bdc8569508f865109776da5972e452ef))


### CI

* bump atlassian/bitbucket-upload-file ([b7f789f](https://bitbucket.org/baosystems/baosystems.postgresql/commits/b7f789fe2e47cbdd3cc9aceeec9e87e8c2ead3e6))
* disable CentOS 8 in EC2 ([725ed8f](https://bitbucket.org/baosystems/baosystems.postgresql/commits/725ed8f87ddcfe958cb14282598e910c2170d6ac))
* fix clientonly scenario ([d3e7b72](https://bitbucket.org/baosystems/baosystems.postgresql/commits/d3e7b72e023143bc9e309b9702c8e0cb88873a15))
* increase git clone depth ([6aa0ba6](https://bitbucket.org/baosystems/baosystems.postgresql/commits/6aa0ba6fa69ff31c0728f3f1c5051807f1787676))


### Tests

* add version suffix and remove lint ([3528b24](https://bitbucket.org/baosystems/baosystems.postgresql/commits/3528b24e1d5cb128c0fdd7b43ea532d6c8d1dfab))
* append version to platform names ([fc60363](https://bitbucket.org/baosystems/baosystems.postgresql/commits/fc60363c21ea4f338f74cda4d3c869d8116bea0e))


### Others

* improve comment ([90999b2](https://bitbucket.org/baosystems/baosystems.postgresql/commits/90999b269867bf6e3ecbe7344710cc9bcb821a9a))

## [1.0.4](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.4%0D1.0.3#diff) (2020-10-19)


### Bug Fixes

* bump geerlingguy.repo-epel to fix EL8 problem ([2ce6634](https://bitbucket.org/baosystems/baosystems.postgresql/commits/2ce6634c34b3ab72ad42fcf834c698fdd59d8556))
* fix ansible-lint 208 ([4696dfb](https://bitbucket.org/baosystems/baosystems.postgresql/commits/4696dfb5710c3861241d364826278da0c201ccc2))
* prefer python2-psycopg2 on CentOS 7 ([d5edaff](https://bitbucket.org/baosystems/baosystems.postgresql/commits/d5edaff2683db58677edea40a2ca8cba2ff00dd6))


### Reverts

* use previous release of geerlingguy.repo-epel ([e652431](https://bitbucket.org/baosystems/baosystems.postgresql/commits/e652431bebe930c3d23ea562d54c0a8cc0d8a152))


### Tests

* add scenario and tests for PostgreSQL 13 ([33c01f3](https://bitbucket.org/baosystems/baosystems.postgresql/commits/33c01f30a01fe2c42f981e732239be4539cb7fcd))


### CI

* **molecule:** official CentOS AMIs ([f2e4219](https://bitbucket.org/baosystems/baosystems.postgresql/commits/f2e42192938676bcda2a3a71724be79ca6df885f))
* add role name to allow for other roles to run in parallel ([8f823dd](https://bitbucket.org/baosystems/baosystems.postgresql/commits/8f823ddde5f93a2c05313e1ea13fd7e18eb606a0))
* bump atlassian/bitbucket-upload-file ([1573584](https://bitbucket.org/baosystems/baosystems.postgresql/commits/15735842c485880875d342bfa507826d62dfeeb7))
* improve pipelines and use yaml anchors ([d0eec23](https://bitbucket.org/baosystems/baosystems.postgresql/commits/d0eec2329ab33260b42d82e33d17d32a1378f547))
* include LICENSE in archives ([a30ded2](https://bitbucket.org/baosystems/baosystems.postgresql/commits/a30ded253700a0f9a27ee2723df262cd178efd57))
* remove ec2/prepare.yml ([4685f4c](https://bitbucket.org/baosystems/baosystems.postgresql/commits/4685f4cf2cb894f08720e5956a2aa57fde6994de))
* use centos images for multi-arch support ([1658ad6](https://bitbucket.org/baosystems/baosystems.postgresql/commits/1658ad68bea9aa153a8c388ef38edfd47e5e53c1))


### Others

* fix comment prefix ([15d3b9b](https://bitbucket.org/baosystems/baosystems.postgresql/commits/15d3b9b8fd336e5f22585e9635a9675b4630e79f))
* fix typo ([9f59912](https://bitbucket.org/baosystems/baosystems.postgresql/commits/9f59912f1ae1b413fe55710fad0d34eb8b1dfb45))


### Build System

* update python packages ([208293f](https://bitbucket.org/baosystems/baosystems.postgresql/commits/208293f2dbd365f2849093c52ac485fabbfacdeb))
* **pre-commit:** bump hooks ([fbb341f](https://bitbucket.org/baosystems/baosystems.postgresql/commits/fbb341fc66944fc5bae79ebc116389f980ba29e0))

## [1.0.3](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.3%0D1.0.2#diff) (2020-09-16)


### Bug Fixes

* **tasks/pgdg:** add PGDG GPG keys before adding the repository ([ca5a8c3](https://bitbucket.org/baosystems/baosystems.postgresql/commits/ca5a8c38edf02b27e300718abf375a5dc7fa1a59))


### Styling

* **tasks/pgdg:** consistent comments ([944d7b1](https://bitbucket.org/baosystems/baosystems.postgresql/commits/944d7b105210ea77c9c8e87309b8e2eaf7e1bea1))

## [1.0.2](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.2%0D1.0.1#diff) (2020-09-15)


### Bug Fixes

* **tasks/pgdg:** use official repo for aarch64 ([930b8de](https://bitbucket.org/baosystems/baosystems.postgresql/commits/930b8de216afddc8e0fc736b46dd9fdec154e330))


### Tests

* remove architecture-specific tests ([221b532](https://bitbucket.org/baosystems/baosystems.postgresql/commits/221b532268a541ff966d550089e0ec03bfa67132))
* **test_maintenance:** update test to use same URL from 917255fa ([9e5ceb0](https://bitbucket.org/baosystems/baosystems.postgresql/commits/9e5ceb0dd3e8f3d97bc3e76595ddf2453be36549))


### CI

* **molecule:** use t4g.medium for ARM instances ([1ada951](https://bitbucket.org/baosystems/baosystems.postgresql/commits/1ada9519563bbb402783c7c94131a761ac7d1ce2))


### Others

* **tasks/maintenance:** update repo rpm URL ([917255f](https://bitbucket.org/baosystems/baosystems.postgresql/commits/917255fa30787c013f91f10f273a7578c7e6f959))

## [1.0.1](https://bitbucket.org/baosystems/baosystems.postgresql/compare/1.0.1%0D1.0.0#diff) (2020-09-10)


### Bug Fixes

* **pgdg:** update baseurl for BAO PGDG repository ([67ed4b7](https://bitbucket.org/baosystems/baosystems.postgresql/commits/67ed4b7ff3653d22a3490cf1c7c9de6365524b60))


### CI

* run systemd without privileged ([50b65a0](https://bitbucket.org/baosystems/baosystems.postgresql/commits/50b65a0d7d3ce95ba867b4d1746e627ae174d6c0))
* **molecule:** test centos7_arm now that BAO PGDG packages are built ([3ee30ca](https://bitbucket.org/baosystems/baosystems.postgresql/commits/3ee30ca631fdc5fe31b517c10234099e1c6eca73))


### Tests

* fix 01-logging.conf sha256sum ([15fe2e2](https://bitbucket.org/baosystems/baosystems.postgresql/commits/15fe2e27f27a4ec836c049868fdaf28fa959ef0b))
* podman scenario needs same tests as default ([e049889](https://bitbucket.org/baosystems/baosystems.postgresql/commits/e04988977be9a49373d90cf1e876c8ab37df11b3))


### Docs

* **readme:** improve readme ([f42ba81](https://bitbucket.org/baosystems/baosystems.postgresql/commits/f42ba81e209a26d2d83244bece6f83b3544750c8))
* **readme:** readme formatting ([aaa92b8](https://bitbucket.org/baosystems/baosystems.postgresql/commits/aaa92b80c20d9fbf4091657a6be11daa2b99ce72))
* **readme:** readme formatting ([2e5d3af](https://bitbucket.org/baosystems/baosystems.postgresql/commits/2e5d3afe51484a833f40b8c92024e6a85a48cff5))
* **readme:** typofix ([3313a1f](https://bitbucket.org/baosystems/baosystems.postgresql/commits/3313a1f9333223a97d4ea3c5ddd0acf9c3ca7ca4))
* **readme:** update URL for pgbadger README ([b3e62d4](https://bitbucket.org/baosystems/baosystems.postgresql/commits/b3e62d4ed0dfb662b36ffa4e86c1fb5da9694779))

## 1.0.0 (2020-07-09)


### Features

* import refactored role ([1542662](https://bitbucket.org/baosystems/baosystems.postgresql/commits/15426621250fc1dd809bae64f1269a7006c0d0a0))


### Bug Fixes

* add flake8 to virtualenv ([8614edc](https://bitbucket.org/baosystems/baosystems.postgresql/commits/8614edc0c6527be356dd87b8df2ea548c5ef4dc4))
